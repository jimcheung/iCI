# iCI 查词小工具
> 1. 这是一款利用JavaFx开发的爱词霸词典的查词小工具

![image](http://git.oschina.net/JHuZhang/iCI/raw/master/iCI.jpg)

# 引用
> 1. JSoup:一款Java的HTML解析器，可直接解析某个URL地址、HTML文本内容
> 2. JIntellitype:实现全局热键，Java原生API并不支持为应用程序设置全局热键

# 更新说明
>## 2015-04-09 ##
> 1. 完善查词功能:当输入大写字母时,经常搜索不到
> 2. 完善托盘功能:托盘化窗口初始状态
> 3. 完善全局热键:增加配置热键功能,默认热键更改为 ctrl+1(查询) ctrl+2(显示_隐藏) ctrl+3(退出)

>## 2015-03-30 ##
> 1. 实现查词功能
> 2. 实现拖拽功能
> 3. 实现托盘化
> 4. 实现全局热键:ctrl+q(查询) ctrl+s(显示_隐藏) ctrl+e(退出)

