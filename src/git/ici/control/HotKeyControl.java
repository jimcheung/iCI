package git.ici.control;

import git.ici.ICIManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Properties;

import com.melloware.jintellitype.HotkeyListener;
import com.melloware.jintellitype.JIntellitype;

/**
 * 实现系统级热键功能
 * 
 * @author Jinhu
 * @date 2015年3月29日
 */

public class HotKeyControl {
	// 定义热键标识，用于在设置多个热键时，在事件处理中区分用户按下的热键
	public static final int SHOW_HIDE_KEY_MARK = 1;
	public static final int QUERY_KEY_MARK = 2;
	public static final int EXIT_KEY_MARK = 3;

	public static char SHOW_HIDE_KEY = '1';
	public static char QUERY_KEY = '2';
	public static char EXIT_KEY = '3';

	private static boolean IS_SHOWED = true;

	private WindowControl window;

	static {
		loadProperites();
	}

	public HotKeyControl(WindowControl window) {
		this.window = window;
	}

	/**
	 * 注册系统级热键
	 */
	public void register() {
		JIntellitype.getInstance().registerSwingHotKey(SHOW_HIDE_KEY_MARK,
				JIntellitype.MOD_CONTROL, (int) SHOW_HIDE_KEY);
		JIntellitype.getInstance().registerHotKey(QUERY_KEY_MARK,
				JIntellitype.MOD_CONTROL, (int) QUERY_KEY);
		JIntellitype.getInstance().registerHotKey(EXIT_KEY_MARK,
				JIntellitype.MOD_CONTROL, (int) EXIT_KEY);
		respondHotKey();
	}

	/**
	 * 注销热键
	 */
	public void unregisterHotKey(int keyNo) {
		JIntellitype.getInstance().unregisterHotKey(keyNo);
	}

	/**
	 * 注册一个热键
	 *
	 * @param hotkey
	 * @param mark
	 *            任务标记
	 */
	public void registerAHotKey(int mark, char hotkey) {
		JIntellitype.getInstance().registerHotKey(mark,
				JIntellitype.MOD_CONTROL, (int) hotkey);
	}

	/**
	 * 响应热键
	 */
	public void respondHotKey() {
		JIntellitype.getInstance().addHotKeyListener(new HotkeyListener() {
			@Override
			public void onHotKey(int mark) {
				switch (mark) {
				case SHOW_HIDE_KEY_MARK:
					if (IS_SHOWED) {
						window.hide();
						IS_SHOWED = false;
					} else {
						window.show();
						IS_SHOWED = true;
					}
					break;
				case QUERY_KEY_MARK:
					if (!IS_SHOWED) {
						window.show();
						IS_SHOWED = true;
					}
					window.turnToQueryPane();
					break;
				case EXIT_KEY_MARK:
					window.exit();
					break;
				}
			}
		});
	}

	/**
	 * 配置默认热键
	 * 
	 * @throws IOException
	 */
	public static void loadProperites() {
		try {
			Path path = Paths.get(ICIManager.Properties);
			InputStream inputStream = Files.newInputStream(path,
					StandardOpenOption.READ);

			Properties pro = new Properties();
			pro.load(inputStream);

			SHOW_HIDE_KEY = pro
					.getProperty("SHOW_HIDE_KEY", SHOW_HIDE_KEY + "").charAt(0);
			QUERY_KEY = pro.getProperty("QUERY_KEY", QUERY_KEY + "").charAt(0);
			EXIT_KEY = pro.getProperty("EXIT_KEY", EXIT_KEY + "").charAt(0);

			inputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 储存热键配置
	 */
	public static void saveProperites() {
		try {
			Path path = Paths.get(ICIManager.Properties);
			OutputStream outputStream = Files.newOutputStream(path,
					StandardOpenOption.WRITE);

			Properties pro = new Properties();
			pro.setProperty("SHOW_HIDE_KEY", SHOW_HIDE_KEY + "");
			pro.setProperty("QUERY_KEY", QUERY_KEY + "");
			pro.setProperty("EXIT_KEY", EXIT_KEY + "");
			pro.store(outputStream, "HotKey_Property");

			outputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
