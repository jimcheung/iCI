package git.ici.control;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * 实现程序的托盘化
 * 
 * @author Jinhu
 * @date 2015年3月28日
 */

public class TrayControl {

	private WindowControl window;

	private SystemTray systemTray;
	private TrayIcon trayIcon;
	private MenuItem showItem;
	private MenuItem minimumItem;
	private MenuItem setItem;
	private MenuItem exitItem;

	public TrayControl(WindowControl window) {
		this.window = window;
	}

	/**
	 * 构造托盘图标，并将其绑定菜单
	 * 
	 * @throws IOException
	 */
	private void initTrayIcon() throws IOException {
		showItem = new MenuItem("显示");
		minimumItem = new MenuItem("最小");
		setItem = new MenuItem("配置");
		exitItem = new MenuItem("退出");

		PopupMenu menu = new PopupMenu();
		menu.add(showItem);
		menu.add(minimumItem);
		menu.add(setItem);
		menu.add(exitItem);

		Image image = ImageIO.read(TrayControl.class
				.getResourceAsStream("/resources/image/tray.png"));

		trayIcon = new TrayIcon(image, "iCI", menu);
	}

	/**
	 * 注册菜单项和托盘的监听器
	 */
	private void initEvent() {

		// 显示程序
		showItem.addActionListener(event -> {
			window.show();
		});

		// 最小化
		minimumItem.addActionListener(event -> {
			window.iconified();
		});

		// 配置热键
		setItem.addActionListener(event -> {
			window.showConfigureDialog();
		});

		// 退出程序
		exitItem.addActionListener(event -> {
			systemTray.remove(trayIcon);
			window.exit();
		});

		// 双击托盘图标显示程序
		trayIcon.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (MouseEvent.BUTTON1 == e.getButton()
						&& 2 == e.getClickCount()) {
					window.show();
				}
			}
		});
	}

	/**
	 * 将构造好的托盘图标加入系统托盘中
	 */
	public void toSystemTray() {
		try {
			systemTray = SystemTray.getSystemTray();

			initTrayIcon();
			initEvent();

			systemTray.add(trayIcon);
		} catch (AWTException | IOException e) {
			e.printStackTrace();
		}
	}

}
