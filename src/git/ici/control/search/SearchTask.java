package git.ici.control.search;

import git.ici.control.SearchControl;
import git.ici.model.Word;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * 搜索单词的任务
 * 
 * @author Jinhu
 * @date 2015年3月29日
 */

public class SearchTask implements Callable<Word> {

	private Word word;
	private String letter;

	private Document doc;

	// 匹配音频文件url
	private Pattern pattern;
	private static final String regex = "(http://.*\\.(mp3|MP3))";

	public SearchTask(String letter) {
		this.letter = letter;
		word = new Word(letter);
		pattern = Pattern.compile(regex);
	}

	/**
	 * 连接爱词霸，获取html代码
	 * 
	 * @param url
	 * @return
	 * @throws IOException
	 */
	private boolean connect(String url) {
		try {
			doc = Jsoup.connect(url).get();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * 解析html
	 * 
	 * @return
	 */
	private boolean parse() {
		char c = letter.charAt(0);

		/**
		 * 中文的查找
		 * 
		 * 与英语查找有些区别
		 */
		if (c >= '\u4e00' && c <= '\u9fa5') {
			int i = 0;
			Elements elmes = doc.select("div[class=group_prons]");
			for (Element e : elmes) {
				Element se = e.select("span[class=label_list").first();
				if (se != null) {
					String key = "中" + i;
					String values = se.text();
					if (values.length() > 0)
						word.getMeaning().put(key, values);
				}
				i++;
			}

		} else {

			/**
			 * 英文的查找
			 */
			// 获得单词意思组
			Element meaningGroup = doc.select("div[class^=group_po]").first();

			Elements partOfSpeech = meaningGroup.select("p");
			for (Element p : partOfSpeech) {
				// 词性
				String key = "", values = "";
				key = p.select("strong[class^=fl]").first().text();
				values = p.select("span[class^=label_]").text();
				word.getMeaning().put(key, values);
			}
		}

		// 获得音标
		Elements phoneticGroup = doc.select("span[class^=eg]");
		for (Element p : phoneticGroup) {
			String text = "";
			text = p.text();
			if (text.startsWith("[")) {
				word.getPhonetic().put("中", text);
			} else {
				String key = "", value = "";
				key = String.valueOf(text.charAt(0));
				value = text.substring(1);
				word.getPhonetic().put(key, value);
			}
		}

		// 获得音频
		Element sound = doc.select("div[class^=vCri]").first();
		if (sound != null) {
			String soundurl = "";
			soundurl = sound.select("a").first().attr("onclick");
			word.setPronUrl(matcher(soundurl));
		}
		return true;
	}

	/**
	 * 匹配音频url
	 * 
	 * @param soundurl
	 * @return 音频url
	 */
	private String matcher(String soundurl) {
		Matcher matcher = pattern.matcher(soundurl);
		if (matcher.find())
			return matcher.group(1);
		else
			return null;
	}

	/**
	 * 任务执行的主体
	 */
	@Override
	public Word call() throws Exception {
		String url = SearchControl.url + letter;
		word.setUrl(url);
		boolean connected = connect(url);
		if (!connected) {
			System.out.println("Fail Connection");
			return null;
		}
		parse();
		return word;
	}

}
