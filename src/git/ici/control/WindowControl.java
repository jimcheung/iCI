package git.ici.control;

import git.ici.ICIManager;
import git.ici.model.Word;
import git.ici.view.ICIFrame;
import git.ici.view.ResultDialog;
import javafx.application.Platform;
import javafx.scene.effect.Light.Point;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;

/**
 * 处理窗体动作的控制器
 * 
 * @author Jinhu
 * @date 2015年3月28日
 */

public class WindowControl {
	private ICIManager manager;
	private Stage stage;
	private ICIFrame iciFrame;
	private ResultDialog resultDialog;

	private static final Point point;
	private static boolean IsDrag;
	private static boolean IsRightDownInTextFiled;

	static {
		point = new Point();
		IsDrag = false;
	}

	public WindowControl(ICIManager manager) {
		this.manager = manager;
		this.stage = manager.getStage();
		this.resultDialog = manager.getResultDialog();
		this.iciFrame = manager.getICIFrame();
		initWindowEvent();
	}

	/**
	 * 注册组件监听器,初始化窗体事件
	 */
	private void initWindowEvent() {
		/**
		 * 拖拽窗体
		 */
		manager.getTitleLabel().setOnMousePressed(event -> {
			point.setX(iciFrame.getX() - event.getScreenX());
			point.setY(iciFrame.getY() - event.getScreenY());
			IsDrag = false;
		});

		manager.getTitleLabel().setOnMouseDragged(event -> {
			double X = point.getX() + event.getScreenX();
			double Y = point.getY() + event.getScreenY();
			Platform.runLater(() -> {
				iciFrame.setX(X);
				iciFrame.setY(Y);
				// 如果对话框显示了,将其隐藏
				if (resultDialog.isShowing()) {
					Platform.setImplicitExit(false);
					resultDialog.hide();
				}
			});
			IsDrag = true;
		});

		/**
		 * 标题面板转到输入面板
		 */
		manager.getTitleLabel().setOnMouseClicked(event -> {
			if (IsDrag)
				return;
			if (manager.getContentBox().isVisible())
				return;
			turnToQueryPane();
		});

		/**
		 * 输入面板转到标题面板
		 */
		manager.getContentBox().setOnMouseExited(event -> {
			if (IsRightDownInTextFiled)
				return;
			Platform.runLater(() -> {
				manager.getTitleBox().setVisible(true);
				manager.getContentBox().setVisible(false);
				manager.getTitleBox().setFocusTraversable(true);
			});
			IsRightDownInTextFiled = false;
		});

		/**
		 * 解决文本框右击菜单和切换面板冲突
		 */
		manager.getTextFiled().setOnMousePressed(event -> {
			if (event.isSecondaryButtonDown()) {
				IsRightDownInTextFiled = true;
			} else
				IsRightDownInTextFiled = false;
		});

		/**
		 * 窗口托盘化
		 */
		manager.getClosedButton().setOnAction(event -> {
			hide();
		});

		/**
		 * 最小化
		 */
		manager.getMinimumButton().setOnAction(event -> {
			iconified();
		});

		/**
		 * 搜索事件触发
		 */
		manager.getSearchButton().setOnAction(event -> {
			String letter = manager.getTextFiled().getText().toLowerCase();
			if (letter == null || letter.length() == 0)
				return;
			SearchControl.start(letter);
			Word word = SearchControl.get();
			// System.out.println(word.getLetter() + "   " + word.getUrl());
			// System.out.println(word.getMeaning());
			// System.out.println(word.getPhonetic());
			// System.out.println(word.getPronUrl());
				resultDialog.acceptWord(letter, word);
				resultDialog.clean();
				resultDialog.update();
				resultDialog.show(iciFrame.getX(), iciFrame.getY());
			});

		/**
		 * 文本框按下Enter键触发搜索按钮动作
		 */
		manager.getTextFiled().setOnKeyPressed(event -> {
			if (event.getCode() == KeyCode.ENTER) {
				manager.getSearchButton().fire();
			}
		});
	}

	/**
	 * 退出程序
	 */
	public void exit() {
		Platform.runLater(() -> {
			iciFrame.close();
			resultDialog.close();
		});
		// 当前线程休眠1s,使Platform的工作线程完成
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		HotKeyControl.saveProperites();
		Platform.exit();
		System.exit(0);
	}

	/**
	 * 显示窗口
	 */
	public void show() {
		Platform.runLater(() -> {
			iciFrame.show();
		});
	}

	/**
	 * 最小化窗口
	 */
	public void iconified() {
		Platform.runLater(() -> {
			iciFrame.setIconified(true);
		});
	}

	/**
	 * 隐藏窗口
	 */
	public void hide() {
		// stage。hide和close方法等价,需调用以下方法取消绝对退出
		Platform.setImplicitExit(false);
		Platform.runLater(() -> {
			iciFrame.hide();
			resultDialog.hide();
		});
	}

	/**
	 * 转到搜索面板
	 */
	public void turnToQueryPane() {
		Platform.runLater(() -> {
			manager.getTitleBox().setVisible(false);
			manager.getContentBox().setVisible(true);
			manager.getTextFiled().setText("");
			manager.getTextFiled().setFocusTraversable(true);
		});
	}

	/**
	 * 显示配置框
	 */
	public void showConfigureDialog() {
		Platform.runLater(() -> {
			manager.getConfigureDialog().show();
		});
	}

}
