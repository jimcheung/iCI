package git.ici.control;

import git.ici.ICIManager;

/**
 * 主控制器:负责监听视图动作,并指派任务给子控制器
 * 
 * @author Jinhu
 * @date 2015年3月27日
 */

public class MainControl {

	private WindowControl window;
	private TrayControl tray;
	private HotKeyControl hotKey;
	private ConfigureControl configure;

	private ICIManager manager;

	public MainControl(ICIManager manager) {
		this.manager = manager;
	}

	/**
	 * 初始化
	 */
	public void init() {
		initSubControl();
		initAction();
	}

	/**
	 * 初始化子控制器
	 */
	private void initSubControl() {
		window = new WindowControl(manager);
		tray = new TrayControl(window);
		hotKey = new HotKeyControl(window);
		configure = new ConfigureControl(manager);

		configure.acceptHotKeyControl(hotKey);
	}

	/**
	 * 构建子控制器的动作
	 */
	private void initAction() {
		// 加入系统托盘
		tray.toSystemTray();

		// 注册热键
		hotKey.register();
	}

}
