package git.ici.control;

import git.ici.ICIManager;
import git.ici.view.ConfigureDialog;

/**
 * 程序配置信息的控制器
 * 
 * @author Jinhu
 * @date 2015年4月8日
 */

public class ConfigureControl {

	private ICIManager manager;
	private HotKeyControl hotKeyControl;

	public ConfigureControl(ICIManager manager) {
		this.manager = manager;
		initEvent();
	}

	/**
	 * 初始事件
	 */
	private void initEvent() {
		manager.getConfigureDialog().getSubmitButton().setOnAction(event -> {
			setHotkey();
			manager.getConfigureDialog().hide();
		});
	}

	/**
	 * 设置热键
	 */
	private void setHotkey() {
		ConfigureDialog configureDialog = manager.getConfigureDialog();

		String text = configureDialog.getShowHKField().getText();
		if (text != null && text.length() > 0) {
			char showKey = text.toUpperCase().charAt(0);
			if (showKey != HotKeyControl.SHOW_HIDE_KEY) {
				hotKeyControl.unregisterHotKey(1);
				hotKeyControl.registerAHotKey(HotKeyControl.SHOW_HIDE_KEY_MARK,
						showKey);
				HotKeyControl.SHOW_HIDE_KEY = showKey;
				configureDialog.getShowHKField().setText(showKey + "");
			}
		}

		text = configureDialog.getQueryHKField().getText();
		if (text != null && text.length() > 0) {
			char queryKey = text.toUpperCase().charAt(0);
			if (queryKey != HotKeyControl.QUERY_KEY) {
				hotKeyControl.unregisterHotKey(2);
				hotKeyControl.registerAHotKey(HotKeyControl.QUERY_KEY_MARK,
						queryKey);
				HotKeyControl.QUERY_KEY = queryKey;
				configureDialog.getQueryHKField().setText(queryKey + "");
			}
		}

		text = configureDialog.getExitHKField().getText();
		if (text != null && text.length() > 0) {
			char exitKey = text.toUpperCase().charAt(0);
			if (exitKey != HotKeyControl.EXIT_KEY) {
				hotKeyControl.unregisterHotKey(3);
				hotKeyControl.registerAHotKey(HotKeyControl.EXIT_KEY_MARK,
						exitKey);
				HotKeyControl.EXIT_KEY = exitKey;
				configureDialog.getExitHKField().setText(exitKey + "");
			}
		}

	}

	/**
	 * 接受热键控制器
	 * 
	 * @param hotKeyControl
	 */
	public void acceptHotKeyControl(HotKeyControl hotKeyControl) {
		this.hotKeyControl = hotKeyControl;
	}
}
