package git.ici.control;

import git.ici.control.search.SearchTask;
import git.ici.model.Word;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * 搜索单词控制器,基于爱词霸搜索引擎
 * 
 * @author Jinhu
 * @date 2015年3月29日
 */

public class SearchControl {
	public static final String url = "http://www.iciba.com/";

	private static final ExecutorService threadPool = Executors
			.newCachedThreadPool();

	private static Future<Word> task;

	/**
	 * 启动搜索任务
	 * 
	 * @param word
	 *            被搜索的单词
	 */
	public static synchronized void start(String letter) {
		task = threadPool.submit(new SearchTask(letter));
	}

	/**
	 * 获得搜索结果
	 * 
	 * task完成前,此方法会阻塞
	 * 
	 * @return 单词
	 * 
	 */
	public static synchronized Word get() {
		Word word = null;
		try {
			word = task.get();
		} catch (InterruptedException | ExecutionException e) {
			word = null;
			e.printStackTrace();
		}
		return word;
	}
}
