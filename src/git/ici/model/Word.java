package git.ici.model;

import java.util.HashMap;

/**
 * 封装单词数据
 * 
 * @author Jinhu
 * @date 2015年3月25日
 */

public class Word {
	// 搜索地址
	private String url;
	// 拼写
	private String letter;
	// 音频url
	private String pronUrl;
	// 意思
	private HashMap<String, String> meaning;
	// 音标
	private HashMap<String, String> phonetic;

	public Word(String letter) {
		this.letter = letter;
		meaning = new HashMap<String, String>();
		phonetic = new HashMap<String, String>();
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getLetter() {
		return letter;
	}

	public void setLetter(String letter) {
		this.letter = letter;
	}

	public String getPronUrl() {
		return pronUrl;
	}

	public void setPronUrl(String pronUrl) {
		this.pronUrl = pronUrl;
	}

	public HashMap<String, String> getMeaning() {
		return meaning;
	}

	public HashMap<String, String> getPhonetic() {
		return phonetic;
	}
}
