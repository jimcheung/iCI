package git.ici;

import git.ici.control.MainControl;
import git.ici.view.ConfigureDialog;
import git.ici.view.ICIFrame;
import git.ici.view.ResultDialog;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * 管理程序中的数据交互
 * 
 * @author Jinhu
 * @date 2015年3月25日
 */

public class ICIManager {

	public static final String Properties = "iCI.properties";

	private Stage stage;
	private ICIFrame iciFrame;
	private ResultDialog resultDialog;
	private ConfigureDialog configureDialog;

	private MainControl mainControl;

	/**
	 * 创建配置文件
	 */
	static {
		try {
			Path path = Paths.get(Properties);
			if (!Files.exists(path)) {
				Files.createFile(path);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ICIManager(Stage stage) {
		this.stage = stage;
		this.stage.initStyle(StageStyle.UTILITY);

		iciFrame = new ICIFrame();
		iciFrame.initOwner(stage);
		resultDialog = new ResultDialog();
		resultDialog.initOwner(stage);
		configureDialog = new ConfigureDialog();
		configureDialog.initOwner(stage);

		mainControl = new MainControl(this);
		mainControl.init();
	}

	public Label getTitleLabel() {
		return iciFrame.getTitleLabel();
	}

	public Button getClosedButton() {
		return iciFrame.getClosedButton();
	}

	public Button getMinimumButton() {
		return iciFrame.getMinimumButton();
	}

	public HBox getTitleBox() {
		return iciFrame.getTitleBox();
	}

	public TextField getTextFiled() {
		return iciFrame.getTextFiled();
	}

	public Button getSearchButton() {
		return iciFrame.getSearchButton();
	}

	public HBox getContentBox() {
		return iciFrame.getContentBox();
	}

	public Stage getStage() {
		return stage;
	}

	public ResultDialog getResultDialog() {
		return resultDialog;
	}

	public ICIFrame getICIFrame() {
		return iciFrame;
	}

	public ConfigureDialog getConfigureDialog() {
		return configureDialog;
	}
}
