package git.ici.view;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * 程序主面板
 * 
 * @author Jinhu
 * @date 2015年3月25日
 */

public class ICIFrame extends Stage {

	private static final String title = "iCI";
	public static final int HEIGHT = 38;
	public static final int WIDTH = 345;

	public static final double X = 1000;
	public static final double Y = 700;

	private Scene root;

	private Label titleLabel;
	private Button closedButton;
	private Button minimumButton;
	private HBox titleBox;

	private TextField textFiled;
	private Button searchButton;
	private HBox contentBox;

	private StackPane stackPane;

	public ICIFrame() {
		initComponent();
		initLayout();

		setX(X);
		setY(Y);
		setTitle(title);
		setScene(root);
		setFullScreen(false);
		setResizable(false);
		initStyle(StageStyle.TRANSPARENT);
		// show();
	}

	/**
	 * 初始化组件
	 */
	private void initComponent() {
		titleLabel = new Label(" Hello");
		closedButton = new Button();
		minimumButton = new Button();
		titleBox = new HBox();

		textFiled = new TextField();
		searchButton = new Button();
		contentBox = new HBox();

		stackPane = new StackPane();
		root = new Scene(stackPane, WIDTH, HEIGHT);
	}

	/**
	 * 构造布局
	 */
	private void initLayout() {
		// containBox的构造
		textFiled.setPrefSize(WIDTH - HEIGHT, HEIGHT);
		searchButton.setPrefSize(HEIGHT, HEIGHT);

		textFiled.setPromptText("爱词查词");
		Image image = new Image(
				ICIFrame.class
						.getResourceAsStream("/resources/image/search.png"));
		searchButton.setGraphic(new ImageView(image));

		contentBox.getChildren().addAll(textFiled, searchButton);
		contentBox.setVisible(false);

		// titleBox的构造
		titleLabel.setPrefSize(WIDTH - 40, HEIGHT);
		closedButton.setPrefSize(20, 20);
		minimumButton.setPrefSize(20, 20);

		image = new Image(
				ICIFrame.class
						.getResourceAsStream("/resources/image/minimize.png"));
		minimumButton.setGraphic(new ImageView(image));
		image = new Image(
				ICIFrame.class
						.getResourceAsStream("/resources/image/close.png"));
		closedButton.setGraphic(new ImageView(image));

		titleLabel.setId("title-Label");
		searchButton.setId("search-button");

		titleBox.setFocusTraversable(true);
		titleBox.getChildren().addAll(titleLabel, minimumButton, closedButton);

		// stackPane加入Box
		stackPane.getChildren().addAll(contentBox, titleBox);

		// 加入CSS属性
		root.getStylesheets().add(
				ICIFrame.class.getResource("/resources/css/iCIFrame.css")
						.toExternalForm());
	}

	public Label getTitleLabel() {
		return titleLabel;
	}

	public Button getClosedButton() {
		return closedButton;
	}

	public Button getMinimumButton() {
		return minimumButton;
	}

	public HBox getTitleBox() {
		return titleBox;
	}

	public TextField getTextFiled() {
		return textFiled;
	}

	public Button getSearchButton() {
		return searchButton;
	}

	public HBox getContentBox() {
		return contentBox;
	}

}
