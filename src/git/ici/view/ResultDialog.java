package git.ici.view;

import git.ici.model.Word;

import java.util.ArrayList;
import java.util.List;

import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * 展示搜索单词信息的对话框
 * 
 * @author Jinhu
 * @date 2015年3月29日
 */

public class ResultDialog extends Stage {

	public static final int HEIGHT = 250;
	public static final int WIDTH = 345;

	private Scene root;
	private VBox contentBox;

	private HBox textBox;
	private Text text;

	private HBox pronBox;
	private VBox meaningBox;

	private Word word;
	private String letter;

	public ResultDialog() {
		initComponent();
		initLayout();
		initCSS();
		setResizable(false);
		setFullScreen(false);
		setScene(root);
		initStyle(StageStyle.TRANSPARENT);

	}

	/**
	 * 初始组件
	 */
	private void initComponent() {
		contentBox = new VBox();

		textBox = new HBox();
		text = new Text();

		pronBox = new HBox();

		meaningBox = new VBox();
		root = new Scene(contentBox, WIDTH, HEIGHT);
	}

	/**
	 * 初始布局
	 */
	private void initLayout() {
		textBox.setPrefHeight(40);
		pronBox.setPrefHeight(40);

		textBox.getChildren().add(text);

		contentBox.getChildren().addAll(textBox, pronBox, meaningBox);

	}

	/**
	 * 初始CSS
	 */
	private void initCSS() {
		textBox.setId("text-box");
		text.setId("title-text");

		pronBox.setId("pron-box");

		contentBox.setId("contentBox");

		root.getStylesheets().add(
				ResultDialog.class.getResource(
						"/resources/css/resultDialog.css").toExternalForm());
	}

	/**
	 * 更新对话框的位置,并显示
	 * 
	 * @param X
	 * @param Y
	 *            主窗口的坐标
	 */
	public void show(double X, double Y) {
		double y = Y - HEIGHT;
		if (y < 0)
			y = Y + ICIFrame.HEIGHT;
		setX(X);
		setY(y);
		Platform.runLater(() -> {
			show();
		});
	}

	/**
	 * 接受一个单词对象
	 * 
	 * @param word
	 */
	public void acceptWord(String letter, Word word) {
		this.letter = letter;
		this.word = word;
	}

	public boolean update() {
		if (word == null) {
			text.setText("对不起，没有找到'" + letter + "'");
			return false;
		}

		String title = word.getLetter();

		Platform.runLater(() -> {
			text.setText(" " + title);
			pronBox.getChildren().addAll(getPhoneticLabels());
			meaningBox.getChildren().addAll(getMeaningLabels());
			// if (word.getPronUrl() != null && word.getPronUrl().length() > 0)
			// {
			// pronBox.getChildren().add(new Button(" "));
			// }

		});
		return true;
	}

	/**
	 * 
	 * @return 音标标签组
	 */
	private List<Label> getPhoneticLabels() {
		List<Label> list = new ArrayList<Label>();
		word.getPhonetic().forEach((key, value) -> {
			if (key.length() > 0 && value.length() > 0) {
				String str = "";
				if (key.startsWith("中"))
					str = value;
				else
					str = key + value;
				list.add(new Label(" " + str));
			}
		});
		return list;
	}

	/**
	 * 
	 * @return 意思标签组
	 */
	private List<Label> getMeaningLabels() {
		List<Label> list = new ArrayList<Label>();
		word.getMeaning().forEach((key, value) -> {
			if (key.length() > 0 && value.length() > 0) {
				String str = "";
				if (key.startsWith("中"))
					str = " " + value;
				else
					str = " " + key + " " + value;

				list.add(new Label(str));
			}
		});
		return list;
	}

	public void clean() {
		if (text.getText().length() <= 0)
			return;
		Platform.runLater(() -> {
			text.setText("");
			pronBox.getChildren().clear();
			meaningBox.getChildren().clear();
		});

	}
}
