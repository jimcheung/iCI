package git.ici.view;

import git.ici.control.HotKeyControl;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * 配置框:设置热键
 * 
 * @author Jinhu
 * @date 2015年4月8日
 */

public class ConfigureDialog extends Stage {

	private Scene root;
	private VBox rootPane;

	private TitledPane hotkeyTitlePane;
	private VBox hotkeyContentPane;

	private Text showHKTip;
	private Text queryHKTip;
	private Text exitHKTip;

	private TextField showHKField;
	private TextField queryHKField;
	private TextField exitHKField;

	private final String modkey = "Ctrl";
	private Button submitButton;

	public ConfigureDialog() {
		initComponent();
		initLayout();

		setScene(root);
		setTitle("配置");
		initStyle(StageStyle.TRANSPARENT);
	}

	/**
	 * 初始组件
	 */
	private void initComponent() {
		rootPane = new VBox();

		hotkeyContentPane = new VBox();
		hotkeyTitlePane = new TitledPane("热键", hotkeyContentPane);
		showHKTip = new Text("显示:   ");
		queryHKTip = new Text("查询:   ");
		exitHKTip = new Text("退出:   ");

		showHKField = new TextField(HotKeyControl.SHOW_HIDE_KEY + "");
		queryHKField = new TextField(HotKeyControl.QUERY_KEY + "");
		exitHKField = new TextField(HotKeyControl.EXIT_KEY + "");

		submitButton = new Button("确定");

		root = new Scene(rootPane);
	}

	/**
	 * 初始布局
	 */
	private void initLayout() {
		// 设置热键布局
		GridPane g1 = new GridPane();
		GridPane g2 = new GridPane();

		g1.setAlignment(Pos.CENTER);
		g1.setPadding(new Insets(10, 10, 10, 10));
		g1.setVgap(10);
		g1.setHgap(2);

		g1.add(showHKTip, 0, 0);
		g1.add(new Text(modkey + "+"), 1, 0);
		g1.add(showHKField, 2, 0, 3, 1);

		g1.add(queryHKTip, 0, 1);
		g1.add(new Text(modkey + "+"), 1, 1);
		g1.add(queryHKField, 2, 1, 3, 1);

		g1.add(exitHKTip, 0, 2);
		g1.add(new Text(modkey + "+"), 1, 2);
		g1.add(exitHKField, 2, 2, 3, 1);

		g2.setAlignment(Pos.BOTTOM_RIGHT);
		g2.setPadding(new Insets(0, 10, 10, 10));
		g2.add(submitButton, 1, 1);

		hotkeyContentPane.getChildren().addAll(g1, g2);

		rootPane.getChildren().add(hotkeyTitlePane);
	}

	/**
	 * 隐藏窗口
	 */
	public void hide() {
		Platform.setImplicitExit(false);
		Platform.runLater(() -> {
			super.hide();
		});
	}

	public TextField getShowHKField() {
		return showHKField;
	}

	public TextField getQueryHKField() {
		return queryHKField;
	}

	public TextField getExitHKField() {
		return exitHKField;
	}

	public Button getSubmitButton() {
		return submitButton;
	}

}
