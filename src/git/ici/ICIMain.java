package git.ici;

import javafx.application.Application;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * 基于爱词霸词典的桌面应用
 * 
 * @author Jinhu
 * @date 2015年3月25日
 */

public class ICIMain extends Application {

	@Override
	public void start(Stage stage) throws Exception {
		new ICIManager(stage);
	}

	public static void main(String[] args) {
		launch(args);
	}
}
